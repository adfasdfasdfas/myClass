//es6语法：'
// require("../src/style.css");
import Vue from "../node_modules/vue/dist/vue.min.js";
import VueRouter from "../node_modules/vue-router/dist/vue-router.js";

import Routers from './router';

// import index from "../src/components/app.vue";
// import list from '../src/components/list.vue';
// import home from '../src/components/home.vue';	

Vue.config.debug = true;//开启错误提示
Vue.use(VueRouter);

var test = Vue.extend({});

var router = new VueRouter();

router.map(Routers);

router.redirect({
    '*':"/login"
});
// var app = require('../app');
router.start(test, '#app1');