/**
 * Created by aresn on 16/8/22.
 */
// const routers = {
//     '/index': {
//         component (resolve) {
//             require(['./views/index.vue'], resolve);
//         }
//     }
// };

const routers = {
    '/login':{
        name : 'login',
        component (resolve) {
            require(['../src/components/login'], resolve);
        }
    },
    '/basic' :{
    	name : 'basic',
    	component (resolve) {
            require(['../src/components/basic'], resolve);
        }
    },
    '/home':{
        name : 'home',
        component (resolve) {
            require(['../src/components/index'], resolve);
        }
    },
    '/map':{
        name : 'map',
        component (resolve) {
            require(['../src/components/map'], resolve);
        }
    },
    '/':{
        name : 'app',
        component (resolve) {
            require(['../app'], resolve);
        }
    }
}


export default routers;