var path = require('path');
var webpack = require('webpack');
// var ExtractTextPlugin = require('extract-text-webpack-plugin');
// var HtmlWebpackPlugin = require('html-webpack-plugin');
// NodeJS中的Path对象，用于处理目录的对象，提高开发效率。
// 模块导入
module.exports = {
    // 入口文件地址，不需要写完，会自动查找
    entry: {
        main:'./src/main',
    },
    // 输出
    output: {
        path: path.join(__dirname, './dist'),
        // 文件地址，使用绝对路径形式
        filename: '[name].js',
        //[name]这里是webpack提供的根据路口文件自动生成的名字
        publicPath: '/dist/'
        // 公共文件生成的地址
    },
    // 服务器配置相关，自动刷新!
    devServer: {
        historyApiFallback: true,
        hot: false,
        inline: true,
        grogress: true,
    },
    // 加载器
    module: {
        // 加载器
        // loaders: [
        // // 解析.vue文件
        //     { test: /\.vue$/, loader: 'vue' },
        // // 转化ES6的语法
        //     { test: /\.js$/, loader: 'babel', exclude: /node_modules/ },
        // // 编译css并自动添加css前缀
        //     { test: /\.css$/, loader: 'style!css!autoprefixer'},
        // //.scss 文件想要编译，scss就需要这些东西！来编译处理
        // //install css-loader style-loader sass-loader node-sass --save-dev
        //     { test: /\.scss$/, loader: 'style!css!sass?sourceMap'},
        // // 图片转化，小于8K自动转化为base64的编码
        //     // { test: /\.(png|jpg|gif)$/, loader: 'url-loader?limit=8192'},
        //     { test: /\.(gif|jpg|png|woff|svg|eot|ttf)\??.*$/, loader: 'url-loader?limit=8192'},
        // //html模板编译？
        //     { test: /\.(html|tpl)$/, loader: 'html-loader' },

        //     { test: /iview\\.*?js$/, loader: 'babel' },// for Windows

        // ]
        loaders: [{
            test: /\.vue$/,
            loader: 'vue'
        },  
        { 
            test: /iview\\.*?js$/,
            loader: 'babel' },// for Windows
        {
            test: /\.js$/,
            loader: 'babel',
            exclude: /node_modules/
        }, 
        {
            test: /\.css$/,
            loader: 'vue-style-loader!css-loader'
        }, 
        { 
            test: /\.scss$/, 
            loader: 'style!css!sass?sourceMap'},
        {
            test: /\.less$/,
            loader: 'vue-style-loader!css-loader!less-loader'
        },  
        { 
            test: /\.(gif|jpg|png|woff|svg|eot|ttf)\??.*$/, 
            loader: 'url-loader?limit=8192'
        },
        //html模板编译？
        {
            test: /\.(html|tpl)$/,
            loader: 'html-loader'
        }]
    },
    // .vue的配置。需要单独出来配置
   
    // 转化成es5的语法
    babel: {
        presets: ['es2015'],
        plugins: ['transform-runtime']
    },
    resolve: {
        // require时省略的扩展名，如：require('module') 不需要module.js
        extensions: ['', '.js', '.vue'],
        // 别名，可以直接使用别名来代表设定的路径以及其他
        alias: {
            components: path.join(__dirname, './src/components'),
            jquery:path.join(__dirname,  './src/lib/js/jquery.js')
        }
    },
    externals: {
        $: "jquery",
        jQuery: "jquery",
        "window.jQuery": "jquery"
    },
     plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: "jquery",
            "window.jQuery": "jquery"
        }),
    ]
    // 开启source-map，webpack有多种source-map，在官网文档可以查到
    // devtool: 'eval-source-map'
};