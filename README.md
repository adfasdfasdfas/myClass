
#vue+ webpack 实践

实例 ：http://1.hjhvue1.applinzi.com/#!/

#处理 jquery，和插件的引用 的

    resolve: {
        // require时省略的扩展名，如：require('module') 不需要module.js
        extensions: ['', '.js', '.vue'],
        // 别名，可以直接使用别名来代表设定的路径以及其他
        alias: {
            components: path.join(__dirname, './src/components'),
            jquery:path.join(__dirname,  './src/lib/js/jquery.js')
        }
    },
    externals: {
        $: "jquery",
        jQuery: "jquery",
        "window.jQuery": "jquery"
    },
     plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: "jquery",
            "window.jQuery": "jquery"
        }),
    ]


运行项目

#myClass

#1st step

npm install

#2nd step

npm run init

#3rd step
npm run dev

#4th step

open url on pc bower: localhost:8081